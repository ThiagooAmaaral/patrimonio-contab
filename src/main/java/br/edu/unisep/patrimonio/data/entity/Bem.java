package br.edu.unisep.patrimonio.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_bem")
public class Bem {

    @Id
    @Column(name = "idbem")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idbem;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "valor")
    private Double valor;


}
