package br.edu.unisep.patrimonio.data.repository;

import br.edu.unisep.patrimonio.data.entity.Obrigacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObrigacaoRepository extends JpaRepository<Obrigacao, Integer> {

}
