package br.edu.unisep.patrimonio.data.repository;


import br.edu.unisep.patrimonio.data.entity.Direito;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DireitoRepository extends JpaRepository<Direito, Integer> {

}
